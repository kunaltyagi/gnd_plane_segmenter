# gnd_plane_segmenter

Contains nodes and nodelets for ground plane detection as well as detection of obstacles on a generic plane.

Node and nodelets have the same interface and same parameters. Easiest is to launch the nodelet.xml with parameter `tf2_robot` as true incase there is tf publisher for `base_link`. The input would be (at **/camera/aligned_depth_to_color/image_raw**) the depth image and it's corresponding camera information and output would be (at **/solid**) a point cloud of detected obstacles

## tf_pointcloud
Converts a point cloud from one frame to another. Uses ROS TF2 for this purpose.
### Parameters
* **queue_size**: <type: int32, default: 5> size of subscriber queue
* **target_frame**: <type: string, default: "map"> convert point cloud to this frame
### Topics
* **/points**: <type: sensor_msgs/PointCloud2> input point cloud
* **/tf_points**: <type: sensor_msgs/PointCloud2> output point cloud

## plane_detector
Detects a plane perpendicular to Z axis (wherever that may be). It works by
1. Finding 2 planes: one near the user and one further away.
2. If the 2 planes are not within a relaxation angle-cone, nothing is done
3. The 2 sets of parameters are converted to spherical coordinates and filtered using an exponential filter
4. The 2 filtered parameters are averaged based on the number of points in the linier set
5. The averaged parameters are converted back to cartesian coordinates

Near plane's near threshold is 0

### Parameters
* **queue_size**: <type: int32, default: 5> size of subscriber queue
* **coarsness**: <type: double, default: 0.05> in meters, the size of "thickness" of the detected plane
* **far_factor**: <type: double, default: 1.5> The far plane is wider by this factor
* **far_threshold**: <type: double, default: 7.5> The max distance points are valid for consideration in far plane
* **filter_factor**: <type: double, default: 0.2> The weight of current value in exponential filter
* **max_height**: <type: double, default: 1.0> Height from 0 for the consideration of points
* **min_inliers**: <type: double, default: 1.5> Minimum number of inliers in the 2 planes in total for a valid computation
* **near_threshold**: <type: double, default: 1.5> Near threshold for far plane and far threshold for near plane
* **overlap**: <type: double, default: 0.25> Fraction of overlap starting from 'near_threshold' to 0 for the far plane
* **relaxation**: <type: double, default: 15 degrees> in radian, the cone of acceptance between the near and far plane normals
* **witdh**: <type: double, default: 0.75> The width limit (+ and -) for near plane inliers. For far plane inliers, the actual value is 'far_factor * width'
### Topics
* **/points**: <type: sensor_msgs/PointCloud2> input point cloud
* **/gnd_plane**: <type:gnd_plane_segmenter/Plane> detected plane
* **/gnd_near_plane**: <type:sensor_msgs/PointCloud2> (debugging) near plane
* **/gnd_far_plane**: <type:sensor_msgs/PointCloud2> (debugging) far plane

## plane_verifier
Maintains a [Boolinger Band](https://en.wikipedia.org/wiki/Bollinger_Bands) around each of the 4 plane parameters.

If 3 or 4 values are outside the envelope, the values are limited to band limits. The mean and band limits are recalculated every time.
### Parameters
* **queue_size**: <type: int32, default: 5> size of subscriber queue
* **band_width**: <type: doublem default: 2.5> std-dev multiplier to define the band width
* **history**: <type: int32, default: 30> moving average for calculating the mean and variance
* **min_confidence**: <type: double, default: 0.5> minimum confidence to accept a plarameter set
### Topics
* **/plane_in**: <type: gnd_plane_segmenter/Plane> input
* **/plane_out**: <type: gnd_plane_segmenter/Plane> output
* **/plane_statistics**: <type: gnd_plane_segmenter/PlaneStatistics> plane parameters for others to use (and debugging)

## plane_visualizer
Helps visualize the plane in rviz

@TODO:
* better naming of topics
* more parameters
* perhaps better names for parameters
### Parameters
* **queue_size**: <type: int32, default: 5> size of subscriber queue
* **width**: <type: double, default: 0.1> z size of unrotated plane
* **height**: <type: double, default: 0.1> y size of unrotated plane
* **length**: <type: double, default: 0.1> x size of unrotated plane
### Topics
* **/plane**: <type: gnd_plane_segmenter/Plane> input
* **/gnd_plane_coeff**: <type: geometry_msgs/PoseStamped> plane parameters
* **/gnd_plane_marker**: <type: visualization_msgs/Marker> plane for display

## obstacle_detector
Takes in a point cloud and a plane and finds all obstacles within a control volume (as projected onto the plane)

@TODO:
* holes is currently the detected ground plane. Change it from that to detected lack of points (aka holes in ground)
### Parameters
* **queue_size**: <type: int32, default: 5> size of subscriber queue
* **coarsness**: <type: double, default: 0.1> thickness of points for not considering them as obstacles
* **width**: <type: double, default: 0.5> control volume x size: -width to +width
* **height**: <type: double, default: 1.0> control volume z size: anything to +height
* **length**: <type: double, default: 2.0> control volume y size: anything to +length
### Topics
* **/points**: <type: sensor_msgs/PointCloud2> input point cloud
* **/plane**: <type: gnd_plane_segmenter/Plane> input plane
* **/solid**: <type: sensor_msgs/PointCloud2> detected obstacles
* **/hole**: <type: sensor_msgs/PointCloud2> detected ground plane (not holes :/, sad but true)

#ifndef _GND_PLANE_SEGMENTER_POINT_CLOUD_OPERATIONS_HPP_
#define _GND_PLANE_SEGMENTER_POINT_CLOUD_OPERATIONS_HPP_

#include <optional>
#include <tuple>
#include <type_traits>

#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <rbot_utils/algorithm.hpp>
#include <rbot_utils/math.hpp>
#include <rbot_utils/pointer.hpp>
#include <rbot_utils/transform.hpp>

namespace rbot_utils {
namespace v1 {
template <
    class ValueType,
    class CloudType,
    class = std::enable_if_t<is_compute_base<CloudType>::value>,
    class = std::enable_if_t<std::is_same<ValueType, pcl::PointXYZ>::value>>
constexpr ValueType average(const CloudType &pointCloud) noexcept(true)
{
    ValueType sum(0, 0, 0);
    if (pointCloud.size() == 0) {
        return sum;
    }

    for (const auto &pt : pointCloud) {
        sum.x += pt.x;
        sum.y += pt.y;
        sum.z += pt.z;
    }
    sum.x = sum.x / pointCloud.size();
    sum.y = sum.y / pointCloud.size();
    sum.z = sum.z / pointCloud.size();
    return sum;
}

template <
    class ValueType,
    class CloudType,
    class = std::enable_if_t<is_compute_base<CloudType>::value>,
    class = std::enable_if_t<std::is_same<ValueType, pcl::PointXYZ>::value>>
constexpr ValueType variance(const CloudType &pointCloud) noexcept(true)
{
    ValueType sum(0, 0, 0);
    if (pointCloud.size() == 0) {
        return sum;
    }

    ValueType avg = average<ValueType>(pointCloud);
    for (const auto &pt : pointCloud) {
        auto diff = avg.x - pt.x;
        sum.x += diff * diff;

        diff = avg.y - pt.y;
        sum.y += diff * diff;

        diff = avg.z - pt.z;
        sum.z += diff * diff;
    }
    sum.x = sum.x / pointCloud.size();
    sum.y = sum.y / pointCloud.size();
    sum.z = sum.z / pointCloud.size();
    return sum;
}
static_assert(is_compute_base<pcl::PointCloud<pcl::PointXYZ>>::value);
}  // namespace v1
}  // namespace rbot_utils

namespace gnd_plane_segmenter {
/**
 * @tparam  T type of point cloud
 * @param negative if true,  returns points outside (min-max) range
 *                 if false, returns points outside (min-max) range
 */
template <class T>
std::shared_ptr<T> filterMinMax(std::shared_ptr<T> inputCloud,
                                std::string fieldName,
                                double min,
                                double max,
                                bool negative = false) noexcept(false)
{
    auto output = std::make_shared<T>();
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud(rbot_utils::to_boost_shared_ptr(inputCloud));
    pass.setFilterFieldName(fieldName);
    pass.setFilterLimits(min, max);
    pass.setNegative(negative);
    pass.filter(*output);

    return output;
}

template <class T>
std::shared_ptr<T> extractCloud(std::shared_ptr<T> inputCloud,
                                std::shared_ptr<pcl::PointIndices> inliers,
                                bool negative = false) noexcept(false)
{
    if (!inputCloud || !inliers) {
        return {};
    }

    auto output = std::make_shared<T>();
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    extract.setInputCloud(rbot_utils::to_boost_shared_ptr(inputCloud));
    extract.setIndices(rbot_utils::to_boost_shared_ptr(inliers));
    extract.setNegative(negative);
    extract.filter(*output);

    return output;
}

/**
 * @tparam T type of point cloud
 * @parameter coarseness distance between point and plane during fitting
 * @return [unit_normal, distance_from_origin, indices_of_inliers]
 */
template <class T>
auto filterPlane(std::shared_ptr<T> inputCloud,
                 double coarseness = 0.1,
                 Eigen::Vector3f normal = Eigen::Vector3f::UnitX(),
                 double relaxation = rbot_utils::deg2rad(15.)) noexcept(false)
    -> std::optional<
        std::tuple<Eigen::Vector3f, float, std::shared_ptr<pcl::PointIndices>>>
{
    if (inputCloud == nullptr) {
        return {};
    }
    if (inputCloud->size() == 0) {
        return {};
    }

    auto inliers = boost::make_shared<pcl::PointIndices>();
    auto coefficients = std::make_shared<pcl::ModelCoefficients>();
    pcl::SACSegmentation<pcl::PointXYZ> seg;

    seg.setOptimizeCoefficients(true);  // optional
    seg.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
    seg.setAxis(normal);
    seg.setEpsAngle(relaxation);
    seg.setMaxIterations(50);  // default is 50
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setDistanceThreshold(coarseness);
    seg.setInputCloud(rbot_utils::to_boost_shared_ptr(inputCloud));
    seg.segment(*inliers, *coefficients);

    if (inliers->indices.size() == 0) {
        std::cout << "Couldn't estimate planar model\n";
        return {};
    }

    Eigen::Vector3f coeff;
    for (int i = 0; i < 3; ++i) {
        coeff[i] = coefficients->values[i];
    }
    // assert(coeff.norm() == 1.);
    auto d = coefficients->values[3];

    return std::make_tuple(coeff, d, rbot_utils::to_std_shared_ptr(inliers));
}
}  // namespace gnd_plane_segmenter
#endif /* ifndef _GND_PLANE_SEGMENTER_POINT_CLOUD_OPERATIONS_HPP_ */

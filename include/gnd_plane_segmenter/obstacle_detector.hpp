#ifndef _GND_PLANE_SEGMENTER_OBSTACLE_DETECTOR_HPP_
#define _GND_PLANE_SEGMENTER_OBSTACLE_DETECTOR_HPP_

#include <cmath>

#include <memory>
#include <mutex>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <pcl/ModelCoefficients.h>
#include <pcl/PointIndices.h>
#include <pcl/filters/project_inliers.h>

#include <pcl_conversions/pcl_conversions.h>
#include <ros/ros.h>

#include <sensor_msgs/PointCloud2.h>
#include <std_srvs/Empty.h>

#include <rbot_utils/algorithm.hpp>
#include <rbot_utils/math.hpp>

#include <gnd_plane_segmenter/Plane.h>
#include <gnd_plane_segmenter/point_cloud_operations.hpp>

namespace gnd_plane_segmenter {
template <class BaseClass>
struct ObstacleDetector : public BaseClass
{
    virtual void onInit() override final
    {
        std_srvs::Empty::Request req;
        std_srvs::Empty::Response res;
        m_reloadParams(req, res);

        ros::SubscriberStatusCallback connectCb =
            std::bind(&ObstacleDetector::m_connectCb, this);
        ros::NodeHandle &nh = BaseClass::getNodeHandle();
        ros::NodeHandle &priv_nh = BaseClass::getPrivateNodeHandle();

        m_paramUpdate = priv_nh.advertiseService(
            "update_param", &ObstacleDetector::m_reloadParams, this);
        std::lock_guard<std::mutex> guard(m_connectMutex);
        m_solidPub = nh.advertise<ROS_PC>("solid", 1, connectCb, connectCb);
        m_holePub = nh.advertise<ROS_PC>("hole", 1, connectCb, connectCb);
    }

  protected:
    using Plane = gnd_plane_segmenter::Plane;
    using ROS_PC = sensor_msgs::PointCloud2;
    using PointCloud = pcl::PointCloud<pcl::PointXYZ>;

    ros::Subscriber m_planeSub, m_pointCloudSub;
    ros::Publisher m_holePub, m_solidPub;
    ros::ServiceServer m_paramUpdate;
    std::mutex m_connectMutex;
    double m_coarseness, m_width, m_length, m_height;
    ROS_PC::ConstPtr m_pointcloud;
    Plane::ConstPtr m_plane;
    int m_qSize = 5;

    void m_planeCb(const Plane::ConstPtr &plane) { m_plane = plane; }
    void m_pointCloudCb(const ROS_PC::ConstPtr &pointcloud)
    {
        m_pointcloud = pointcloud;
        m_pointCloudPlaneCb(m_pointcloud, m_plane);
    }

    void m_pointCloudPlaneCb(const ROS_PC::ConstPtr &pointcloud,
                             const Plane::ConstPtr &plane)
    {
        if (!pointcloud || !plane) {
            return;
        }
        Eigen::VectorXf model_coeff(4);
        pcl::ModelCoefficients::Ptr coeff(new pcl::ModelCoefficients);
        coeff->values.resize(4);
        for (int i = 0; i < 4; ++i) {
            model_coeff[i] = plane->plane.coef[i];
            coeff->values[i] = plane->plane.coef[i];
        }

        auto input_cloud = std::make_shared<PointCloud>();
        pcl::fromROSMsg<pcl::PointXYZ>(*pointcloud, *input_cloud);

        // get the points within collision (some x y and z constraints)
        auto width_filtered = filterMinMax(input_cloud, "x", -m_width, m_width);
        auto width_height_filtered =
            filterMinMax(width_filtered, "z", -FLT_MAX, m_height);
        auto collision_cloud =
            filterMinMax(width_height_filtered, "y", -FLT_MAX, m_length);

        auto inlier = std::make_shared<pcl::PointIndices>();
        inlier->header = input_cloud->header;
        pcl::SampleConsensusModelPerpendicularPlane<pcl::PointXYZ> model(
            rbot_utils::to_boost_shared_ptr(collision_cloud));
        model.selectWithinDistance(model_coeff, m_coarseness, inlier->indices);

        // remove the ground from original cloud
        // @TODO: improve upon the two passes
        auto groundless = extractCloud(collision_cloud, inlier, true);
        auto ground = extractCloud(collision_cloud, inlier, false);

        if (groundless->size()) {

        auto projected_cloud = std::make_shared<PointCloud>();
        pcl::ProjectInliers<pcl::PointXYZ> projection;
        projection.setModelType(pcl::SACMODEL_PERPENDICULAR_PLANE);
        projection.setModelCoefficients(coeff);
        projection.setInputCloud(rbot_utils::to_boost_shared_ptr(groundless));
        projection.filter(*projected_cloud);

        ROS_PC::Ptr solid(new ROS_PC);
        pcl::toROSMsg(*projected_cloud, *solid);
        m_solidPub.publish(solid);
        }

        // @TODO: find how to find holes from variable ground
        // ray cast till projected_cloud
        // Pt. size of 5 cm, find holes in ground
        ROS_PC::Ptr hole(new ROS_PC);
        pcl::toROSMsg(*ground, *hole);
        m_holePub.publish(hole);
    }

    bool m_reloadParams(std_srvs::Empty::Request &,
                        std_srvs::Empty::Response &) noexcept(true)
    {
        ros::NodeHandle &priv_nh = BaseClass::getPrivateNodeHandle();

        priv_nh.param("queue_size", m_qSize, 5);
        if (m_qSize < 1) {
            ROS_ERROR("queue_size needs to be more than 0. Setting to 1.");
        }

        priv_nh.param("coarseness", m_coarseness, 0.1);
        priv_nh.param("width", m_width, 0.5);
        priv_nh.param("height", m_height, 1.);
        priv_nh.param("length", m_length, 2.);

        m_coarseness = std::abs(m_coarseness);

        return true;
    }

    void m_connectCb() noexcept(true)
    {
        std::lock_guard<std::mutex> guard(m_connectMutex);
        if (m_solidPub.getNumSubscribers() == 0 &&
            m_holePub.getNumSubscribers() == 0) {
            m_pointCloudSub.shutdown();
            m_planeSub.shutdown();
        } else if (!static_cast<void *>(m_pointCloudSub)) {
            ros::NodeHandle &nh = BaseClass::getNodeHandle();
            m_pointCloudSub = nh.subscribe(
                "points", m_qSize, &ObstacleDetector::m_pointCloudCb, this);
            m_planeSub = nh.subscribe(
                "plane", m_qSize, &ObstacleDetector::m_planeCb, this);
        }
    }
};
}  // namespace gnd_plane_segmenter
#endif /* ifndef _GND_PLANE_SEGMENTER_OBSTACLE_DETECTOR_HPP_ */

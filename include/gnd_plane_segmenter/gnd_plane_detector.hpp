#ifndef _GND_PLANE_SEGMENTER_GND_PLANE_DETECTOR_HPP_
#define _GND_PLANE_SEGMENTER_GND_PLANE_DETECTOR_HPP_

#include <cmath>
#include <cstdint>

#include <memory>
#include <mutex>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <pcl_conversions/pcl_conversions.h>
#include <ros/ros.h>

#include <sensor_msgs/PointCloud2.h>
#include <std_srvs/Empty.h>

#include <rbot_utils/algorithm.hpp>
#include <rbot_utils/math.hpp>

#include <gnd_plane_segmenter/Plane.h>
#include <gnd_plane_segmenter/filter_plane.hpp>

namespace gnd_plane_segmenter {
template <class BaseClass>
struct GndPlaneDetector : public BaseClass
{
    virtual void onInit() override final
    {
        std_srvs::Empty::Request req;
        std_srvs::Empty::Response res;
        m_reloadParams(req, res);

        ros::SubscriberStatusCallback connectCb =
            std::bind(&GndPlaneDetector::m_connectCb, this);

        ros::NodeHandle &nh = BaseClass::getNodeHandle();
        ros::NodeHandle &priv_nh = BaseClass::getPrivateNodeHandle();

        m_paramUpdate = priv_nh.advertiseService(
            "update_param", &GndPlaneDetector::m_reloadParams, this);
        std::lock_guard<std::mutex> guard(m_connectMutex);
        m_planePub = nh.advertise<gnd_plane_segmenter::Plane>(
            "gnd_plane", 1, connectCb, connectCb);
        m_nearPub =
            nh.advertise<ROS_PC>("gnd_near_plane", 1, connectCb, connectCb);
        m_farPub =
            nh.advertise<ROS_PC>("gnd_far_plane", 1, connectCb, connectCb);
    }

  protected:
    using PointCloud = pcl::PointCloud<pcl::PointXYZ>;
    using ROS_PC = sensor_msgs::PointCloud2;

    gnd_detector detector;

    ros::Subscriber m_pointCloudSub;
    ros::Publisher m_nearPub, m_farPub, m_planePub;
    ros::ServiceServer m_paramUpdate;
    std::mutex m_connectMutex;
    std::int32_t m_qSize = 5;

    void m_pointCloudCb(const ROS_PC::ConstPtr &msg)
    {
        if (!msg) {
            return;
        }
#define ROS_CLOCK_WRONG false
#if ROS_CLOCK_WRONG
        auto now = ros::Time::now();
#endif

        auto input_cloud = std::make_shared<PointCloud>();
        pcl::fromROSMsg<pcl::PointXYZ>(*msg, *input_cloud);

        // ASSUMPTION of z vector here
        auto ret_val = detector.detectGndPlane(input_cloud);
        m_debug(ret_val);

        if (!ret_val.plane.has_value()) {
            bool error_near = static_cast<bool>(ret_val.details.near);
            bool error_far = static_cast<bool>(ret_val.details.far);
            ROS_ERROR_STREAM("Ground detection status: Near("
                             << error_near << "), Far(" << error_far << ")");
            return;
        }
        auto &plane = ret_val.plane.value();

        if (plane.confidence) {
            gnd_plane_segmenter::Plane::Ptr planeMsg(
                new gnd_plane_segmenter::Plane);
            planeMsg->header = msg->header;
            planeMsg->publish_time = ros::Time::now();
            for (auto i = 0; i < 3; ++i) {
                planeMsg->plane.coef[i] = plane.normal[i];
            }
            planeMsg->plane.coef[3] = plane.distance;
            planeMsg->confidence = plane.confidence;

#if ROS_CLOCK_WRONG
            planeMsg->header.stamp = now;
#endif
            m_planePub.publish(planeMsg);
        }
    }

    void m_debug(detected_plane filter)
    {
        if (filter.details.near && m_nearPub.getNumSubscribers()) {
            auto near_plane =
                extractCloud(filter.details.near_cloud, filter.details.near);

            ROS_PC::Ptr nearCloud(new ROS_PC);
            pcl::toROSMsg(*near_plane, *nearCloud);
            m_nearPub.publish(nearCloud);
        }
        if (filter.details.far && m_farPub.getNumSubscribers()) {
            auto far_plane =
                extractCloud(filter.details.far_cloud, filter.details.far);

            ROS_PC::Ptr farCloud(new ROS_PC);
            pcl::toROSMsg(*far_plane, *farCloud);
            /* pcl::toROSMsg(*filter.details.far_cloud, *farCloud); */
            m_farPub.publish(farCloud);
        }
    }

    bool m_reloadParams(std_srvs::Empty::Request &,
                        std_srvs::Empty::Response &) noexcept(true)
    {
        ros::NodeHandle &priv_nh = BaseClass::getPrivateNodeHandle();
        double filter_factor;

        priv_nh.param("queue_size", m_qSize, static_cast<std::int32_t>(5));
        if (m_qSize < 1) {
            ROS_ERROR("queue_size needs to be more than 0. Setting to 1.");
        }

        priv_nh.param("coarseness", detector.coarseness, 0.05);
        priv_nh.param("far_factor", detector.farFactor, 1.5);
        priv_nh.param("far_threshold", detector.farThreshold, 7.5);
        priv_nh.param("filter_factor", filter_factor, 0.2);
        priv_nh.param("max_height", detector.maxHeight, 1.);
        priv_nh.param("min_inliers", detector.minInliers, 10000.);
        priv_nh.param("near_threshold", detector.nearThreshold, 3.5);
        priv_nh.param("overlap", detector.overlapFactor, 0.25);
        priv_nh.param(
            "relaxation", detector.relaxation, rbot_utils::deg2rad(15.));
        priv_nh.param("width", detector.width, 0.75);

        if (detector.farFactor <= 1) {
            detector.farFactor = 1;
            ROS_ERROR("far_factor needs to be more than 1. Setting value to 1");
        }
        detector.filter.cos_2relax = std::cos(2 * detector.relaxation);
        detector.filter.set_factor(filter_factor);

        return true;
    }

    void m_connectCb() noexcept(true)
    {
        std::lock_guard<std::mutex> guard(m_connectMutex);
        if (m_nearPub.getNumSubscribers() == 0 &&
            m_farPub.getNumSubscribers() == 0 &&
            m_planePub.getNumSubscribers() == 0) {
            m_pointCloudSub.shutdown();
        } else if (!static_cast<void *>(m_pointCloudSub)) {
            ros::NodeHandle &nh = BaseClass::getNodeHandle();
            m_pointCloudSub = nh.subscribe(
                "points", m_qSize, &GndPlaneDetector::m_pointCloudCb, this);
        }
    }
};
}  // namespace gnd_plane_segmenter
#endif /* ifndef _GND_PLANE_SEGMENTER_GND_PLANE_DETECTOR_HPP_ */

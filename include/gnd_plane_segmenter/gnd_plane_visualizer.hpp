#ifndef _GND_PLANE_SEGMENTER_GND_PLANE_VISUALIZER_HPP_
#define _GND_PLANE_SEGMENTER_GND_PLANE_VISUALIZER_HPP_

#include <cstdint>

#include <mutex>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <ros/ros.h>

#include <geometry_msgs/PoseStamped.h>
#include <std_srvs/Empty.h>
#include <visualization_msgs/Marker.h>

#include <rbot_utils/algorithm.hpp>
#include <rbot_utils/math.hpp>

#include <gnd_plane_segmenter/Plane.h>

namespace gnd_plane_segmenter {
template <class BaseClass>
struct GndPlaneVisualizer : public BaseClass
{
    virtual void onInit() override final
    {
        std_srvs::Empty::Request req;
        std_srvs::Empty::Response res;
        m_reloadParams(req, res);

        ros::SubscriberStatusCallback connectCb =
            std::bind(&GndPlaneVisualizer::m_connectCb, this);

        ros::NodeHandle &nh = BaseClass::getNodeHandle();
        ros::NodeHandle &priv_nh = BaseClass::getPrivateNodeHandle();

        m_paramUpdate = priv_nh.advertiseService(
            "update_param", &GndPlaneVisualizer::m_reloadParams, this);
        std::lock_guard<std::mutex> guard(m_connectMutex);
        m_coeffPub = nh.advertise<geometry_msgs::PoseStamped>(
            "gnd_plane_coeff", 1, connectCb, connectCb);
        m_floorMarkerPub = nh.advertise<visualization_msgs::Marker>(
            "gnd_plane_marker", 1, connectCb, connectCb);
    }

  protected:
    Eigen::Vector3f m_planeSize;

    ros::Subscriber m_planeSub;
    ros::Publisher m_coeffPub, m_floorMarkerPub;
    ros::ServiceServer m_paramUpdate;
    std::mutex m_connectMutex;
    std::int32_t m_qSize = 5;

    void m_planeCb(const gnd_plane_segmenter::Plane::ConstPtr &msg)
    {
        if (!msg) {
            return;
        }
        const auto &coeff = msg->plane.coef;
        auto &distance = coeff[3];

        Eigen::Vector3f avg(coeff[0], coeff[1], coeff[2]);
        avg *= distance;

        Eigen::Quaternionf rotation;
        rotation.setFromTwoVectors(Eigen::Vector3f::UnitX(), avg);

        geometry_msgs::PoseStamped::Ptr coefficients(
            new geometry_msgs::PoseStamped);
        coefficients->header = msg->header;

        rbot_utils::copy_4d(coefficients->pose.orientation, rotation);
        rbot_utils::copy_3d(coefficients->pose.position, avg);

        visualization_msgs::Marker::Ptr floor_marker(
            new visualization_msgs::Marker);
        floor_marker->header = coefficients->header;
        floor_marker->ns = "gnd_floor";
        floor_marker->id = 0;
        floor_marker->type = visualization_msgs::Marker::CUBE;
        floor_marker->action = visualization_msgs::Marker::ADD;
        floor_marker->color.a = 0.7;
        floor_marker->color.r = 0.8;
        floor_marker->color.g = 0.3;
        floor_marker->color.b = 0.3;

        rbot_utils::copy_3d(floor_marker->scale, m_planeSize);
        rbot_utils::copy_3d(floor_marker->pose.position, -avg);
        rbot_utils::copy_4d(floor_marker->pose.orientation, rotation);

        m_floorMarkerPub.publish(floor_marker);
        m_coeffPub.publish(coefficients);
    }

    bool m_reloadParams(std_srvs::Empty::Request &,
                        std_srvs::Empty::Response &) noexcept(true)
    {
        ros::NodeHandle &priv_nh = BaseClass::getPrivateNodeHandle();
        double length, width, height;

        priv_nh.param("queue_size", m_qSize, static_cast<std::int32_t>(5));
        if (m_qSize < 1) {
            ROS_ERROR("queue_size needs to be more than 0. Setting to 1.");
        }

        priv_nh.param("normal", length, 0.1);
        priv_nh.param("width", width, 0.1);
        priv_nh.param("length", height, 0.1);

        if (width <= 0) {
            width = 0.1;
            ROS_ERROR("width needs to be more than 0. Setting value to 0.1");
        }
        if (length <= 0) {
            length = 0.1;
            ROS_ERROR("length needs to be more than 0. Setting value to 0.1");
        }
        if (height <= 0) {
            height = 0.1;
            ROS_ERROR("height needs to be more than 0. Setting value to 0.1");
        }
        m_planeSize = Eigen::Vector3f(length, height, width);

        return true;
    }

    void m_connectCb() noexcept(true)
    {
        std::lock_guard<std::mutex> guard(m_connectMutex);
        if (m_coeffPub.getNumSubscribers() == 0 &&
            m_floorMarkerPub.getNumSubscribers() == 0) {
            m_planeSub.shutdown();
        } else if (!static_cast<void *>(m_planeSub)) {
            ros::NodeHandle &nh = BaseClass::getNodeHandle();
            m_planeSub = nh.subscribe(
                "plane", m_qSize, &GndPlaneVisualizer::m_planeCb, this);
        }
    }
};
}  // namespace gnd_plane_segmenter
#endif /* ifndef _GND_PLANE_SEGMENTER_GND_PLANE_VISUALIZER_HPP_ */

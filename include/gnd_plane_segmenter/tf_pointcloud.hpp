#ifndef _GND_PLANE_SEGMENTER_TF_POINTCLOUD_HPP_
#define _GND_PLANE_SEGMENTER_TF_POINTCLOUD_HPP_

#include <cstdint>

#include <mutex>

#include <pcl_ros/transforms.h>
#include <ros/ros.h>
#include <tf/transform_listener.h>

#include <sensor_msgs/PointCloud2.h>
#include <std_srvs/Empty.h>

namespace gnd_plane_segmenter {
template <class BaseClass>
struct TfPointCloud : public BaseClass
{
  public:
    virtual void onInit() override final
    {
        std_srvs::Empty::Request req;
        std_srvs::Empty::Response res;
        m_reloadParams(req, res);

        ros::SubscriberStatusCallback connectCb =
            std::bind(&TfPointCloud::m_connectCb, this);

        ros::NodeHandle& nh = BaseClass::getNodeHandle();
        std::lock_guard<std::mutex> guard(m_connectMutex);
        m_tfPointCloudPub =
            nh.advertise<PointCloud>("tf_points", 1, connectCb, connectCb);
    }

  protected:
    ros::Subscriber m_pointCloudSub;
    ros::Publisher m_tfPointCloudPub;
    std::mutex m_connectMutex;

    std::int32_t m_qSize;
    std::string m_targetFrame;
    tf::TransformListener m_tfListener;

    using PointCloud = sensor_msgs::PointCloud2;

    void m_connectCb()
    {
        std::lock_guard<std::mutex> guard(m_connectMutex);
        if (m_tfPointCloudPub.getNumSubscribers() == 0) {
            m_pointCloudSub.shutdown();
        } else if (!static_cast<void*>(m_pointCloudSub)) {
            ros::NodeHandle& nh = BaseClass::getNodeHandle();
            m_pointCloudSub = nh.subscribe(
                "points", m_qSize, &TfPointCloud::m_pointCloudCb, this);
        }
    }

    void m_pointCloudCb(const sensor_msgs::PointCloud2ConstPtr& input)
    {
        if (!input) {
            return;
        }
        PointCloud::Ptr output(new PointCloud);

        auto ok = pcl_ros::transformPointCloud(
            m_targetFrame, *input, *output, m_tfListener);

        if (ok) {
            m_tfPointCloudPub.publish(output);
        }
    }

    void m_reloadParams(std_srvs::Empty::Request&, std_srvs::Empty::Response&)
    {
        ros::NodeHandle& priv_nh = BaseClass::getPrivateNodeHandle();

        priv_nh.param("queue_size", m_qSize, static_cast<std::int32_t>(5));
        if (m_qSize < 1) {
            ROS_ERROR("queue_size needs to be more than 0. Setting to 1.");
        }

        priv_nh.param("target_frame", m_targetFrame, std::string("map"));
    }
};
}  // namespace gnd_plane_segmenter
#endif /* ifndef _GND_PLANE_SEGMENTER_TF_POINTCLOUD_HPP_ */

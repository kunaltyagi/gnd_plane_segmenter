#ifndef _GND_PLANE_SEGMENTER_FILTER_PLANE_HPP_
#define _GND_PLANE_SEGMENTER_FILTER_PLANE_HPP_

#include <optional>
#include <tuple>
#include <type_traits>

#include <pcl/filters/project_inliers.h>
#include <pcl/point_types.h>

#include <rbot_utils/algorithm.hpp>
#include <rbot_utils/math.hpp>
#include <rbot_utils/pointer.hpp>
#include <rbot_utils/transform.hpp>

#include <gnd_plane_segmenter/point_cloud_operations.hpp>

namespace gnd_plane_segmenter {
struct FilteredPlane
{
    Eigen::Vector3f normal;
    double distance;
    double confidence;
};
struct Filter2Planes
{
  protected:
    rbot_utils::ExponentialFilter<Eigen::Vector3f> m_nearNormalSph,
        m_farNormalSph;
    rbot_utils::ExponentialFilter<float> m_nearDistance, m_farDistance;
    bool m_firstValue = true;

  public:
    Eigen::Vector3f normal = Eigen::Vector3f::Zero();
    double distance = 0;

    double cos_2relax = 0;

    Filter2Planes(double k_factor = 0.2) noexcept(true)
        : Filter2Planes(0, true, k_factor)
    {}
    Filter2Planes(double angle,
                  bool is_cosine_of_2_times_angle = false,
                  double k_factor = 0.2) noexcept(true)
        : m_nearNormalSph(k_factor, Eigen::Vector3f::Zero()),
          m_farNormalSph(k_factor, Eigen::Vector3f::Zero()),
          m_nearDistance(k_factor),
          m_farDistance(k_factor),
          cos_2relax(is_cosine_of_2_times_angle ? angle : std::cos(2 * angle))
    {}

    void set_factor(double k_factor) noexcept(true)
    {
        m_nearDistance.set_factor(k_factor);
        m_farDistance.set_factor(k_factor);
        m_nearNormalSph.set_factor(k_factor);
        m_farNormalSph.set_factor(k_factor);
    }

    FilteredPlane operator()(double near,
                             double far,
                             Eigen::Vector3f near_coeff,
                             Eigen::Vector3f far_coeff,
                             double near_weight = 0.5) noexcept(true)
    {
        auto near_polar = rbot_utils::cartesian2spherical(near_coeff),
             far_polar = rbot_utils::cartesian2spherical(far_coeff);

        if (m_firstValue) {
            m_nearDistance.reset(near);
            m_farDistance.reset(far);
            m_nearNormalSph.reset(near_polar);
            m_farNormalSph.reset(far_polar);
            m_firstValue = false;
        }

        Eigen::Vector3f near_cartesian, far_cartesian;

        if (near_weight != 0) {
            m_nearDistance.filter(near);
            m_nearNormalSph.filter(near_polar);
            near_cartesian =
                rbot_utils::spherical2cartesian(m_nearNormalSph.data());
        }

        if (near_weight != 1) {
            m_farDistance.filter(far);
            m_farNormalSph.filter(far_polar);
            far_cartesian =
                rbot_utils::spherical2cartesian(m_farNormalSph.data());
        }

        FilteredPlane output;
        if (near_weight == 1 || near_weight == 0) {
            output.confidence = -1;
        } else {
            auto score = near_cartesian.dot(far_cartesian) /
                         m_nearNormalSph.data().x() / m_farNormalSph.data().x();

            output.confidence =
                std::max(0., (score - cos_2relax) / (1 - cos_2relax));
        }

        if (output.confidence) {
            auto avg_polar = near_weight * m_nearNormalSph.data() +
                             (1 - near_weight) * m_farNormalSph.data();
            normal = rbot_utils::spherical2cartesian(avg_polar.eval());
            distance = near_weight * m_nearDistance.data() +
                       (1 - near_weight) * m_farDistance.data();
        }
        output.normal = normal;
        output.distance = distance;

        return output;
    }
};

struct detected_plane
{
    struct indices
    {
        using PointCloud = pcl::PointCloud<pcl::PointXYZ>;
        std::shared_ptr<pcl::PointIndices> near, far;
        std::shared_ptr<PointCloud> near_cloud, far_cloud;
    };
    std::optional<FilteredPlane> plane;
    indices details;
};

struct gnd_detector
{
    double coarseness = 0.05, farFactor = 1.5, farThreshold = 7, maxHeight = 1,
           minInliers = 10000, nearThreshold = 3.5, overlapFactor = 0.25,
           relaxation = rbot_utils::deg2rad(15.), width = 0.75;

    Filter2Planes filter;
    template <class T>
    auto detectGndPlane(std::shared_ptr<T> input_cloud) noexcept(true)
        -> detected_plane
    {
        detected_plane output;
        std::tie(output.details.near_cloud, output.details.far_cloud) =
            filterPointCloud(input_cloud);

        Eigen::Vector3f normal = Eigen::Vector3f::UnitZ();
        auto near_data = filterPlane(
            output.details.near_cloud, coarseness, normal, relaxation);
        auto far_data = filterPlane(output.details.far_cloud,
                                    farFactor * coarseness,
                                    normal,
                                    relaxation);

        if (!near_data.has_value() && !far_data.has_value()) {
            return output;
        }
        double near_d, far_d;
        Eigen::Vector3f near_coeff, far_coeff;

        double total_inliers = 0, numerator = 0;

        if (near_data.has_value()) {
            std::tie(near_coeff, near_d, output.details.near) =
                near_data.value();
            numerator = output.details.near->indices.size();
            total_inliers += output.details.near->indices.size();
        }
        if (far_data.has_value()) {
            std::tie(far_coeff, far_d, output.details.far) = far_data.value();
            total_inliers += output.details.far->indices.size();
        }

        if (total_inliers < minInliers) {
            return output;
        }
        double weight = numerator / total_inliers;
        output.plane = filter(near_d, far_d, near_coeff, far_coeff, weight);
        return output;
    }

    template <class T>
    auto filterPointCloud(std::shared_ptr<T> input_cloud) noexcept(true)
        -> std::tuple<std::shared_ptr<T>, std::shared_ptr<T>>
    {
        /// all of this is being done in robot frame, not camera frame
        // create filter to remove points outside the width from center
        auto x_narrow_filtered_cloud =
            filterMinMax(input_cloud, "x", -width, width);
        auto z_narrow_filtered_cloud =
            filterMinMax(x_narrow_filtered_cloud, "z", -FLT_MAX, maxHeight);
        auto y_near_filtered_cloud =
            filterMinMax(z_narrow_filtered_cloud, "y", 0, nearThreshold);

        auto x_wide_filtered_cloud = filterMinMax(
            input_cloud, "x", -(farFactor * width), farFactor * width);
        auto z_wide_filtered_cloud =
            filterMinMax(x_wide_filtered_cloud, "z", -FLT_MAX, maxHeight);
        auto y_far_filtered_cloud =
            filterMinMax(z_wide_filtered_cloud,
                         "y",
                         (1 - overlapFactor) * nearThreshold,
                         farThreshold);
        return std::make_tuple(y_near_filtered_cloud, y_far_filtered_cloud);
    }
};
}  // namespace gnd_plane_segmenter
#endif /* ifndef _GND_PLANE_SEGMENTER_FILTER_PLANE_HPP_ */

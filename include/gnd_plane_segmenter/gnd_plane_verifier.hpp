#ifndef _GND_PLANE_SEGMENTER_GND_PLANE_VERIFIER_HPP_
#define _GND_PLANE_SEGMENTER_GND_PLANE_VERIFIER_HPP_

#include <cmath>
#include <cstdint>

#include <memory>
#include <mutex>

#include <ros/ros.h>

#include <std_srvs/Empty.h>

#include <Eigen/Dense>

#include <boost/circular_buffer.hpp>

#include <gnd_plane_segmenter/Plane.h>
#include <gnd_plane_segmenter/PlaneStatistics.h>
#include <rbot_utils/math.hpp>

namespace gnd_plane_segmenter {
template <class EigenMatrix>
struct elementWiseMultiplication
{
    EigenMatrix operator()(const EigenMatrix &lhs, const EigenMatrix &rhs)
    {
        return lhs.cwiseProduct(rhs);
    }
};

template <class BaseClass>
struct GndPlaneVerifier : public BaseClass
{
    virtual void onInit() override final
    {
        std_srvs::Empty::Request req;
        std_srvs::Empty::Response res;
        m_reloadParams(req, res);

        ros::SubscriberStatusCallback connectCb =
            std::bind(&GndPlaneVerifier::m_connectCb, this);

        ros::NodeHandle &nh = BaseClass::getNodeHandle();
        ros::NodeHandle &priv_nh = BaseClass::getPrivateNodeHandle();

        m_paramUpdate = priv_nh.advertiseService(
            "update_param", &GndPlaneVerifier::m_reloadParams, this);

        std::lock_guard<std::mutex> guard(m_connectMutex);
        m_cleanedPlanePub = nh.advertise<gnd_plane_segmenter::Plane>(
            "/plane_out", 1, connectCb, connectCb);
        m_planeStatisticsPub =
            priv_nh.advertise<gnd_plane_segmenter::PlaneStatistics>(
                "plane_statistics", 1, connectCb, connectCb);
    }

  protected:
    ros::Subscriber m_planeSub;
    ros::Publisher m_cleanedPlanePub, m_planeStatisticsPub;
    ros::ServiceServer m_paramUpdate;
    std::mutex m_connectMutex;
    std::int32_t m_qSize = 5;

    double m_minConfidence, m_bandWidth;
    rbot_utils::Statistics<boost::circular_buffer<Eigen::Array4f>,
                           Eigen::Array4f,
                           Eigen::Array4f>
        m_buffer;

    void m_planeCb(const gnd_plane_segmenter::Plane::ConstPtr &msg)
    {
        Eigen::Array4f value;
        auto &mean = m_buffer.mean;
        auto &variance = m_buffer.variance;
        const auto &confidence = msg->confidence;
        const auto &diff = value - mean;
        const auto &stddev = variance.sqrt();

        for (auto i = 0; i < 4; ++i) {
            value[i] = msg->plane.coef[i];
        }
        const auto &count =
            m_checkValidity(diff.matrix(), stddev.matrix(), confidence);

        if (m_buffer.buffer.size() != m_buffer.buffer.capacity()) {
            m_buffer.compute(value);
        } else if (count >= 2) {
            m_buffer.compute(value);
        } else {
            Eigen::Array4f result, band = m_bandWidth * stddev;
            rbot_utils::limit(
                value, (mean - band).eval(), (mean + band).eval());
            m_buffer.compute(value);
        }

        gnd_plane_segmenter::Plane::Ptr out(new gnd_plane_segmenter::Plane);
        gnd_plane_segmenter::PlaneStatistics::Ptr debug(
            new gnd_plane_segmenter::PlaneStatistics);

        debug->header = out->header = msg->header;
        debug->publish_time = out->publish_time = ros::Time::now();
        debug->inlier_count = count;
        debug->confidence = confidence;
        for (auto i = 0; i < 4; ++i) {
            out->plane.coef[i] = value[i];
            debug->value.coef[i] = msg->plane.coef[i];
            debug->variance.coef[i] = variance[i];
            debug->mean.coef[i] = mean[i];
        }

        m_cleanedPlanePub.publish(out);
        m_planeStatisticsPub.publish(debug);
    }

    std::size_t m_checkValidity(const Eigen::Array4f &diff,
                                const Eigen::Array4f &stddev,
                                double confidence)
    {
        // -1 is a special value
        if ((confidence != -1) && (confidence < m_minConfidence)) {
            return false;
        }
        const auto &count = (diff.abs() <= (m_bandWidth * stddev)).count();

        return count;
    }

    bool m_reloadParams(std_srvs::Empty::Request &,
                        std_srvs::Empty::Response &) noexcept(true)
    {
        ros::NodeHandle &priv_nh = BaseClass::getPrivateNodeHandle();
        std::int32_t history;

        priv_nh.param("queue_size", m_qSize, static_cast<std::int32_t>(5));
        if (m_qSize < 1) {
            ROS_ERROR("queue_size needs to be more than 0. Setting to 1.");
        }

        priv_nh.param("min_confidence", m_minConfidence, 0.5);
        priv_nh.param("band_width", m_bandWidth, 2.5);
        priv_nh.param("history", history, 30);

        if (m_minConfidence < 0) {
            m_minConfidence = 0.5;
            ROS_ERROR("min_confidence can't be negative. Setting to 0.5");
        } else if (m_minConfidence > 1) {
            m_minConfidence = 0.5;
            ROS_ERROR("min_confidence can't be more than 1. Setting to 0.5");
        }
        if (m_bandWidth < 0) {
            m_bandWidth = 2.5;
            ROS_ERROR("band_width can't be negative. Setting to 2.5");
        }
        if (history < 1) {
            history = 30;
            ROS_ERROR("History needs to be more than 0. Setting value to 50");
        }

        m_buffer.buffer.rset_capacity(history);
        m_buffer.variance = m_buffer.variance.Zero();

        return true;
    }

    void m_connectCb() noexcept(true)
    {
        std::lock_guard<std::mutex> guard(m_connectMutex);
        if (m_cleanedPlanePub.getNumSubscribers() == 0 &&
            m_planeStatisticsPub.getNumSubscribers() == 0) {
            m_planeSub.shutdown();
        } else if (!static_cast<void *>(m_planeSub)) {
            ros::NodeHandle &nh = BaseClass::getNodeHandle();
            m_planeSub = nh.subscribe(
                "plane_in", m_qSize, &GndPlaneVerifier::m_planeCb, this);
        }
    }
};
}  // namespace gnd_plane_segmenter

#endif /* ifndef _GND_PLANE_SEGMENTER_GND_PLANE_VERIFIER_HPP_ */

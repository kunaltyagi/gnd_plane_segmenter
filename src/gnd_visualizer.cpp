#include <rbot_utils/node.hpp>

#include <gnd_plane_segmenter/gnd_plane_visualizer.hpp>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "gnd_display_node");

    gnd_plane_segmenter::GndPlaneVisualizer<rbot_utils::Node<>> displayer;
    displayer.init();

    ros::spin();
    return 0;
}

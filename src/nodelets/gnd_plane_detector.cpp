#ifndef _GND_PLANE_SEGMENTER_NODELET_GND_PLANE_DETECTOR_CPP_
#define _GND_PLANE_SEGMENTER_NODELET_GND_PLANE_DETECTOR_CPP_

#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>

#include <gnd_plane_segmenter/gnd_plane_detector.hpp>

namespace gnd_plane_segmenter {
using GndPlaneDetectorNodelet = GndPlaneDetector<nodelet::Nodelet>;
}  // namespace gnd_plane_segmenter

PLUGINLIB_EXPORT_CLASS(gnd_plane_segmenter::GndPlaneDetectorNodelet,
                       nodelet::Nodelet);
#endif /* ifndef _GND_PLANE_SEGMENTER_NODELET_GND_PLANE_DETECTOR_CPP_ */

#ifndef _GND_PLANE_SEGMENTER_NODELET_GND_PLANE_VISUALIZER_CPP_
#define _GND_PLANE_SEGMENTER_NODELET_GND_PLANE_VISUALIZER_CPP_

#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>

#include <gnd_plane_segmenter/gnd_plane_visualizer.hpp>

namespace gnd_plane_segmenter {
using GndPlaneVisualizerNodelet = GndPlaneVisualizer<nodelet::Nodelet>;
}  // namespace gnd_plane_segmenter

PLUGINLIB_EXPORT_CLASS(gnd_plane_segmenter::GndPlaneVisualizerNodelet,
                       nodelet::Nodelet);
#endif /* ifndef _GND_PLANE_SEGMENTER_NODELET_GND_PLANE_VISUALIZER_CPP_ */

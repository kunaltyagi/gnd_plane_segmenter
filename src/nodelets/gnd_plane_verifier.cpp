#ifndef _GND_PLANE_SEGMENTER_NODELET_GND_PLANE_VERIFIER_CPP_
#define _GND_PLANE_SEGMENTER_NODELET_GND_PLANE_VERIFIER_CPP_

#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>

#include <gnd_plane_segmenter/gnd_plane_verifier.hpp>

namespace gnd_plane_segmenter {
using GndPlaneVerifierNodelet = GndPlaneVerifier<nodelet::Nodelet>;
}  // namespace gnd_plane_segmenter

PLUGINLIB_EXPORT_CLASS(gnd_plane_segmenter::GndPlaneVerifierNodelet,
                       nodelet::Nodelet);
#endif /* ifndef _GND_PLANE_SEGMENTER_NODELET_GND_PLANE_VERIFIER_CPP_ */

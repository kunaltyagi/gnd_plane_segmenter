#ifndef _GND_PLANE_SEGMENTER_NODELET_TF_POINTCLOUD_CPP_
#define _GND_PLANE_SEGMENTER_NODELET_TF_POINTCLOUD_CPP_

#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>

#include <gnd_plane_segmenter/tf_pointcloud.hpp>

namespace gnd_plane_segmenter {
using TfPointCloudNodelet = TfPointCloud<nodelet::Nodelet>;
}  // namespace gnd_plane_segmenter

PLUGINLIB_EXPORT_CLASS(gnd_plane_segmenter::TfPointCloudNodelet,
                       nodelet::Nodelet);
#endif /* ifndef _GND_PLANE_SEGMENTER_NODELET_TF_POINTCLOUD_CPP_ */

#include <rbot_utils/node.hpp>

#include <gnd_plane_segmenter/gnd_plane_detector.hpp>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "gnd_segmentation_node");

    gnd_plane_segmenter::GndPlaneDetector<rbot_utils::Node<>> detector;
    detector.init();

    ros::spin();
    return 0;
}

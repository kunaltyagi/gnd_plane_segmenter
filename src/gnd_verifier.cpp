#include <rbot_utils/node.hpp>

#include <gnd_plane_segmenter/gnd_plane_verifier.hpp>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "gnd_filter_node");

    gnd_plane_segmenter::GndPlaneVerifier<rbot_utils::Node<>> verifier;
    verifier.init();

    ros::spin();
    return 0;
}

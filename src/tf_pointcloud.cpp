#include <rbot_utils/node.hpp>

#include <gnd_plane_segmenter/tf_pointcloud.hpp>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "pointcloud_tf_node");

    gnd_plane_segmenter::TfPointCloud<rbot_utils::Node<>> transformer;
    transformer.init();

    ros::spin();
    return 0;
}

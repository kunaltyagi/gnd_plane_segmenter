#include <rbot_utils/node.hpp>

#include <gnd_plane_segmenter/obstacle_detector.hpp>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "obstacle_detection_node");

    gnd_plane_segmenter::ObstacleDetector<rbot_utils::Node<>> detector;
    detector.init();

    ros::spin();
    return 0;
}
